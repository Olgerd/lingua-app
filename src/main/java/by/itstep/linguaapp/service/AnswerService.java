package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.answers.AnswerFullDto;
import by.itstep.linguaapp.dto.answers.AnswerUpdateDto;
import org.springframework.stereotype.Service;

@Service
public interface AnswerService {

    AnswerFullDto update(AnswerUpdateDto dto);

    void delete (Integer id);

}
