package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answers.AnswerCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.security.AuthenticationService;
import by.itstep.linguaapp.service.QuestionService;
import org.dom4j.io.SAXContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.validation.ValidationException;
import java.time.Instant;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private QuestionMapper questionMapper;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private AuthenticationService authenticationService;

    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto dto) {

        throwIfInvalidNumberOffCorrectAnswers(dto);

        QuestionEntity questionToSave = questionMapper.map(dto);

        for(AnswerEntity answer : questionToSave.getAnswers()){
            answer.setQuestion(questionToSave);
        }

        List<CategoryEntity> categoriesToAdd = categoryRepository
                .findAllById(dto.getCategoryIds());

        questionToSave.setCategories(categoriesToAdd);

        QuestionEntity savedQuestion = questionRepository.save(questionToSave);

        QuestionFullDto questionDto = questionMapper.map(savedQuestion);


        return questionDto;
    }

    private void throwIfInvalidNumberOffCorrectAnswers(QuestionCreateDto dto) {
        int counter = 0;
        for(AnswerCreateDto answer : dto.getAnswers()){
            if(answer.getCorrect()){

                counter++;
            }
        }
            if(counter != 1){
              throw new ValidationException("Question must contains the only one correct answer");
            }

    }

    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto dto) {

        QuestionEntity questionToUpdate = questionRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException("QuestionEntity was not found by id: "
                        + dto.getId()));

        questionToUpdate.setDescription(dto.getDescription());
        questionToUpdate.setLevel(dto.getLevel());

        QuestionEntity updatedQuestion = questionRepository.save(questionToUpdate);
        QuestionFullDto questionDto = questionMapper.map(updatedQuestion);

        System.out.println("QuestionServiceImpl -> Question was successfully updated: " + questionDto);

        return questionDto;
    }

    @Override
    @Transactional
    public QuestionFullDto findById(Integer id) {

        QuestionEntity foundQuestion = questionRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("QuestionEntity was not found find"));

        QuestionFullDto questionDto = questionMapper.map(foundQuestion);
        System.out.println("QuestionServiceImpl -> Question was successfully found " + questionDto);
        return questionDto;
    }

    @Override
    @Transactional
    public List<QuestionShortDto> findAll() {

        List<QuestionEntity> foundQuestions = questionRepository.findAll();

        List<QuestionShortDto> dtos = questionMapper.map(foundQuestions);
        System.out.println("QuestionServiceImpl -> Question was successfully foud" + dtos);

        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {

     QuestionEntity entityToDelete = questionRepository
             .findById(id)
             .orElseThrow(() -> new AppEntityNotFoundException("QuestionEntity was not found find"));

     entityToDelete.setDeletedAt(Instant.now());
      for(AnswerEntity answer : entityToDelete.getAnswers()){
          answer.setDeletedAt((Instant.now()));
      }
      questionRepository.save(entityToDelete);

        System.out.println("QuestionEntity deleted: " + entityToDelete);
    }

    @Override
    @Transactional
    public boolean checkAnsver(Integer questionId, Integer answerId) {

            AnswerEntity answer = answerRepository.findByIdAndQuestionId(answerId, questionId);
            if (answer == null) {
                throw new AppEntityNotFoundException(
                        "Answer was not found by id: " + answerId + " in the question: " + questionId
                );
            }
            if(answer.getCorrect()){
                //
              QuestionEntity question = answer.getQuestion();

              UserEntity user = authenticationService.getAuthenticatedUser();

              question.getUsersWhoCompleted().add(user);
              questionRepository.save(question);
            }


            return answer.getCorrect();
        }

    @Override
    @Transactional
    public QuestionFullDto getRandomQuestion(Integer categoryId) {
        UserEntity user = authenticationService.getAuthenticatedUser();

        List<QuestionEntity> foundQuestions = questionRepository.findNotCompleted(categoryId, user.getId());

        if(foundQuestions.isEmpty()){
            throw new AppEntityNotFoundException("Can`t find available questions by category id: " + categoryId);

        }
        int randomIndex = (int)(foundQuestions.size() * Math.random());


        QuestionEntity randomQuestion = foundQuestions.get(randomIndex);
        System.out.println("QuestionServiceImpl -> Random question was successfully found");

        return questionMapper.map(randomQuestion);
    }
}

