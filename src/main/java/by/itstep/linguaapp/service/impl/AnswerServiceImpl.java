package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answers.AnswerFullDto;
import by.itstep.linguaapp.dto.answers.AnswerUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.BusinessLogicException;
import by.itstep.linguaapp.mapper.AnswerMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
public class AnswerServiceImpl implements AnswerService {
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private AnswerMapper answerMapper;

    @Override
    @Transactional
    public AnswerFullDto update(AnswerUpdateDto dto) {

        AnswerEntity answerToUpdate = answerRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException("Answer was not found by id: " + dto.getId()));

        if(!dto.getCorrect() && answerToUpdate.getCorrect()){
            throw new BusinessLogicException(
                    "Current answer is correct.Set other answer as correct before the update"
            );

        }
        // 1. Если в DTO указано, что ответ является ПРАВИЛЬНЫМ
        if (dto.getCorrect() && !answerToUpdate.getCorrect()) {
            // то мы должны снять галочку с другого правильного ответа
          removeCorrectFlagFromAnswers(answerToUpdate.getQuestion());

        }

        // 2. Обновить ответ
    answerToUpdate.setCorrect(dto.getCorrect());
    answerToUpdate.setBody(dto.getBody());
    AnswerEntity updatedAnswer = answerRepository.save(answerToUpdate);
        System.out.println("AnswerServiceImpl -> Answer was successfully updated");
        return answerMapper.map(updatedAnswer);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        // 1
        AnswerEntity answerToUpdate = answerRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("Answer was not found by id: " + id));

        if(answerToUpdate.getCorrect() && answerToUpdate.getCorrect()){
            throw new BusinessLogicException(
                    "Current answer is correct.Set other answer as correct before the update"
            );
        }
        //2

        if(answerToUpdate.getQuestion().getAnswers().size() < 3){
            throw  new BusinessLogicException("Question must contains at least 2 answers");
        }
        //3
        answerToUpdate.setDeletedAt(Instant.now());
        answerRepository.deleteById(id);

    }

    private void removeCorrectFlagFromAnswers(QuestionEntity question){

        question.getAnswers().stream()
            .filter(answer -> answer.getCorrect())
            .peek(answer -> answer.setCorrect(false))
            .forEach(answer -> answerRepository.save(answer));
    }

}
