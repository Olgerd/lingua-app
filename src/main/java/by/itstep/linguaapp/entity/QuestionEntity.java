package by.itstep.linguaapp.entity;


import liquibase.repackaged.org.apache.commons.lang3.builder.ToStringExclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "questions")
@Where(clause = "deleted_at IS NULL")
public class QuestionEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;


    @Column(name = "description")
    private String description;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "level")
    private QuestionLevel level;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "question", fetch  = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<AnswerEntity> answers = new ArrayList<>();


    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JoinTable(name = "questions_categories",
            joinColumns = {@JoinColumn(name = "question_id")},
            inverseJoinColumns = {@JoinColumn(name = "category_id")}
    )
    @ManyToMany
    private List<CategoryEntity> categories = new ArrayList<>();

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(name="users_completed_questions",
            joinColumns = {@JoinColumn(name="question_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")} )
    private List<UserEntity> usersWhoCompleted = new ArrayList<>();


    @Column(name = "deleted_at")
    private Instant deletedAt;
}
