package by.itstep.linguaapp.controller;


import by.itstep.linguaapp.dto.answers.AnswerCreateDto;
import by.itstep.linguaapp.dto.answers.AnswerFullDto;
import by.itstep.linguaapp.dto.answers.AnswerUpdateDto;
import by.itstep.linguaapp.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
    public class AnswerController {

        @Autowired
        private AnswerService answerService;

        @PutMapping("/answers")
        public AnswerFullDto update(@Valid @RequestBody AnswerUpdateDto updateDto){
            AnswerFullDto updatedAnswer = answerService.update(updateDto);
            return updatedAnswer;
        }

        @DeleteMapping("/answers/id")
        public void delete(@PathVariable int id){
            answerService.delete(id);
        }

}
