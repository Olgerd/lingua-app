package by.itstep.linguaapp.controller;


import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class QuestionController {

    @Autowired
    private QuestionService questionService;


    @GetMapping("/questions/id")
    public QuestionFullDto findById(@PathVariable Integer id){
        return questionService.findById(id);
    }

    @GetMapping("/questions")
    public List<QuestionShortDto> findAllQuestions(){
        List<QuestionShortDto> allQuestions = questionService.findAll();
        return allQuestions;
    }

    @PostMapping("/questions")
    public QuestionFullDto create(QuestionCreateDto dto){
        return questionService.create(dto);
    }

    @PutMapping("/questions")
    public QuestionFullDto update(@Valid @RequestBody QuestionUpdateDto updateDto){
        QuestionFullDto updatedQuestion = questionService.update(updateDto);
        return updatedQuestion;
    }

    @DeleteMapping("/questions/id")
    public void delete(@PathVariable int id){
        questionService.delete(id);
    }

    @PostMapping("/questions/{questionId}/answers/{answerId}/check")
    public boolean checkAnswer(
        @PathVariable Integer questionId,
        @PathVariable Integer answerId){

    return questionService.checkAnsver( questionId,answerId);
    }

    @GetMapping("/categories/{categoryId}/questions/random")
    public QuestionFullDto getRandomQuestion(
            @PathVariable Integer categoryId){

     return questionService.getRandomQuestion(categoryId);


    }
}
