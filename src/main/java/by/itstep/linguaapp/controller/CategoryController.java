package by.itstep.linguaapp.controller;


import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
    public class CategoryController {

    @Autowired
    private CategoryService categoryService;

        @GetMapping("/categories/id")
        public CategoryFullDto findById(@PathVariable Integer id){
            return categoryService.findById(id);
        }

        @GetMapping("/categories")
        public List<CategoryFullDto> findAllCategories(){
            List<CategoryFullDto> allCategories = categoryService.findAll();
            return allCategories;
        }

        @PostMapping("/categories")
        public CategoryFullDto create(@Valid @RequestBody CategoryCreateDto dto){
            return categoryService.create(dto);
        }

        @PutMapping("/categories")
        public CategoryFullDto update(@Valid @RequestBody CategoryUpdateDto updateDto){
            CategoryFullDto updatedCategory = categoryService.update(updateDto);
            return updatedCategory;
        }

        @DeleteMapping("/categories/id")
        public void delete(@PathVariable int id){
            categoryService.delete(id);
        }

    }
