package by.itstep.linguaapp.dto.category;

import by.itstep.linguaapp.entity.QuestionLevel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CategoryUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private  String name;

    @NotNull
    private QuestionLevel level;

}
